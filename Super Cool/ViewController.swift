//
//  ViewController.swift
//  Super Cool
//
//  Created by Jacob Hare on 6/28/16.
//  Copyright © 2016 Jacob Hare. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var uncoolButton: UIButton!
    @IBOutlet weak var UIBg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func ButtonPress(sender: AnyObject) {
        
        uncoolButton.hidden = true
        UIBg.hidden = false
        
        
    }


}

